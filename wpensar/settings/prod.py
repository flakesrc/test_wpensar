import dj_database_url
import django_heroku
from decouple import config
from .base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['http://test-wpensar.herokuapp.com/']

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {'default': dj_database_url.parse(config('DATABASE_URL'))}

django_heroku.settings(locals())