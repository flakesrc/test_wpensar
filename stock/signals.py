from django.db.models.signals import pre_save
from django.dispatch import receiver
from .models import PurchaseModel


# signal para criar preco medio por item
# antes de salvar instancia de compra no banco
@receiver(pre_save, sender=PurchaseModel)
def add_avg_price(sender, instance, **kwargs):
    instance.avg_price = instance.price / instance.quantity
