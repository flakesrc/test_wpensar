from django.db import models


class ProductModel(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


class PurchaseModel(models.Model):
    product = models.ForeignKey(ProductModel, on_delete=models.RESTRICT)
    quantity = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    avg_price = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return f'{self.name} - {self.price}'
