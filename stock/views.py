from django.views.generic import CreateView, ListView
from .models import ProductModel, PurchaseModel
from .forms import ProductForm, PurchaseForm


class ProductCreateView(CreateView):
    """
        Cria um produto
    """
    model = ProductModel
    form_class = ProductForm
    template_name = 'stock/product_create_form.html'
    context_object_name = 'product_form'
    success_url = '/product_create/'


class PurchaseCreateView(CreateView):
    """
        Registra a compra de um produto,
        contendo detalhes da compra
    """
    model = PurchaseModel
    form_class = PurchaseForm
    template_name = 'stock/purchase_create_form.html'
    context_object_name = 'purchase_form'
    success_url = '/purchase_create/'


class PurchaseListView(ListView):
    """
        Exibe lotes comprados
    """
    model = PurchaseModel
    template_name = 'stock/purchase_list.html'
    context_object_name = 'purchase_list'
    ordering = ['-created']
