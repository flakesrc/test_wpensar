from django.urls import path
from .views import ProductCreateView, PurchaseCreateView, PurchaseListView

urlpatterns = [
    path('product_create/', ProductCreateView.as_view()),
    path('purchase_create/', PurchaseCreateView.as_view()),
    path('', PurchaseListView.as_view()),
]
