# Teste WPensar

Os requisitos do teste encontram-se no arquivo `DESCRIPTION.md`
Url da aplicacao em deploy: http://test-wpensar.herokuapp.com/

## Executando o projeto localmente

Projeto criado utilizando Django 3.2.5.

Requisitos:

- python 3.9.6
- pip 20.3.4
- pipenv 2021.5.29

Executando o projeto:

- `git clone https://github.com/flakesrc/wpensar-test.git`
- `pipenv shell`
- `pipenv install`
- `./manage.py migrate`
- `./manage.py runserver`

